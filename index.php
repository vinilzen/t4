<?php
  /*
   *Template Name: index
  */
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Online Магазин Inov-8, купить Inov-8, все о беге, ультра бег, горный бег. Обзоры обуви, одежды и другой экипировки">
    <meta name="author" content="marchukilya@gmail.com">
    <link rel="shortcut icon" href="/ico/favicon.ico">
    <title>TO4EK.NET - Фаза полета | Все о беге, ультра бег, горный бег. Обзоры обуви, одежды и другой экипировки</title>
    <link rel="alternate" type="application/rss+xml" title="Фаза полета &raquo; Лента" href="http://to4ek.net/feed/" />
    <link rel="alternate" type="application/rss+xml" title="Фаза полета &raquo; Лента комментариев" href="http://to4ek.net/comments/feed/" />
    <link href="/css/main.css" rel="stylesheet">
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Фаза полета" />
    <meta property="og:description" content="Все о беге, ультра бег, горный бег. Обзоры обуви, одежды и другой экипировки" />
    <meta property="og:url" content="http://to4ek.net/" />
    <meta property="og:site_name" content="Фаза полета" />
  </head>
  <body>
    <div class="container theme-showcase" role="main">

      <div class="navbar navbar-default" role="navigation">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">TO4EK.NET</a>
          </div>
          <div class="navbar-collapse collapse navbar-right">
            <ul class="nav navbar-nav">
              <li class="active"><a href="/">ГЛАВНАЯ</a></li>
              <li><a href="http://to4ek.net/category/%D1%81%D1%82%D0%B0%D1%82%D1%8C%D0%B8/">СТАТЬИ</a></li>
              <li><a href="http://to4ek.net/category/%D0%BE%D0%B1%D0%B7%D0%BE%D1%80%D1%8B/">ОБЗОРЫ</a></li>
              <li><a href="http://to4ek.net/category/%D0%BD%D0%BE%D0%B2%D0%BE%D1%81%D1%82%D0%B8/">НОВОСТИ</a></li>
              <li class="dropdown">
                <a href="/shop" class="dropdown-toggle" data-toggle="dropdown">МАГАЗИН<b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="http://to4ek.net/shop">Магазин</a></li>
                  <li><a href="http://to4ek.net/catalog/shoes/">Вся обувь</a></li>
                  <li class="divider"></li>
                  <li class="dropdown-header">Обувь по категориям</li>
                  <li><a href="http://to4ek.net/product/oroc/" title="С металичиским шипом для ориентирования и зимы">Oroc</a></li>
                  <li><a href="http://to4ek.net/product/road-x/" title="Обувь для асфальта и бега по дорогам">Road-x</a></li>
                  <li><a href="http://to4ek.net/product/f-lite/" title="Легкие кроссвоки для асфальта и спорт зала">F-lite</a></li>
                  <li><a href="http://to4ek.net/product/x-talon/" title="Шиповки для кроссов и быстрых гонок, ориентирования">X-talon</a></li>
                  <li class="divider"></li>
                  <li><a href="http://to4ek.net/catalog/%D0%BA%D1%83%D1%80%D1%82%D0%BA%D0%B8/">Куртки</a></li>
                </ul>
              </li>
              <li><a href="http://to4ek.net/%D0%BE-%D0%BD%D0%B0%D1%81/">О НАС</a></li>
              <li><a href="http://to4ek.net/%D0%BA%D0%BE%D0%BD%D1%82%D0%B0%D0%BA%D1%82%D1%8B/">КОНТАКТ</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>

      <!-- big top image -->
      <div class="row">
        <div class="bigpic col-md-6 col-sm-6 col-xs-12">
          <a href="http://to4ek.net/%D0%B5%D1%81%D1%82%D0%B5%D1%81%D1%82%D0%B2%D0%B5%D0%BD%D0%BD%D1%8B%D0%B9-%D0%B1%D0%B5%D0%B3/" title="философия inov-8">
            <img class="img-responsive main-big-pic" src="/img/468578521627933.jpg" />
          </a>
        </div>
        <div class="bigpic col-md-3 col-sm-3 col-xs-12">
          <a title="Купить inov-8 trailroc-255" href="http://to4ek.net/shop/inov-8-trailroc-255/">
            <img title="Купить inov-8 trailroc-255" alt="inov-8 trailroc-255" class="img-responsive" src="/img/trailroc-245.jpg" />
          </a>
        </div>
        <div class="bigpic col-md-3 col-sm-3 col-xs-12">
          <a href="http://to4ek.net/trail-running/"><img class="img-responsive" src="/img/forest_morning.jpg" /></a>
        </div>
        <div class="bigpic col-md-3 col-sm-3 col-xs-12">
          <a href="http://to4ek.net/%D0%BF%D0%BE%D0%B4%D0%BE%D1%88%D0%B2%D0%B0-inov-8/"><img class="img-responsive" src="/img/FOOT.jpg" /></a>
        </div>
        <div class="bigpic col-md-3 col-sm-3 col-xs-12">
          <a href="http://to4ek.net/inov-8-%D1%82%D1%80%D0%B8-%D0%BD%D0%B0%D0%B3%D1%80%D0%B0%D0%B4%D1%8B-%D0%BD%D0%B0-ispo/"><img class="img-responsive" src="/img/Race_Ultra_Vest_smallimage.jpg" /></a>
        </div>
      </div>

      <!-- Main jumbotron for a primary marketing message or call to action -->
      <div class="page-header">
        <p class="lead text-center">Добро пожаловать на страничку любителей бега!<br>
          Все о беговой обуви, одежде и остальном снаряжении, отзывы и отчеты о прошедших стартах</p>
      </div>


      <div class="row">
        <div class="col-md-4">
          
          <!-- NEWS -->
          <a href="http://to4ek.net/category/%D0%BD%D0%BE%D0%B2%D0%BE%D1%81%D1%82%D0%B8/" class="btn btn-lg btn-success index-title">НОВОСТИ</a>
          <div class="clearfix"></div>
          <a href="http://to4ek.net/inov-8-%D1%82%D1%80%D0%B8-%D0%BD%D0%B0%D0%B3%D1%80%D0%B0%D0%B4%D1%8B-%D0%BD%D0%B0-ispo/">
            <img src="http://to4ek.net/wp-content/uploads/2014/01/Race_Ultra_Vest_smallimage.jpg" alt="">
          </a>
          <div class="clearfix"></div>
          <a class="title3" href="http://to4ek.net/inov-8-%D1%82%D1%80%D0%B8-%D0%BD%D0%B0%D0%B3%D1%80%D0%B0%D0%B4%D1%8B-%D0%BD%D0%B0-ispo/">У inov-8 три награды на ISPO</a>
          <div class="clearfix"></div>
          <time class="text-muted" datetime="2014-01-20">23 Янв, 2014</time>
          <p>Неустанное стремление к инновациям принесли компании inov-8 три медали на самой крупной международной выставке спортивного снаряжения ISPO. Новый рюкзак-жилет Race Ultra Vest, был признан независимыми экспертами и судьями лучшим в категории «performance equipment» и получил золотую медаль 2014/2015 ISPO AWARD GOLD WINNER.</p>
          <p><a class="btn btn-default" href="http://to4ek.net/inov-8-%D1%82%D1%80%D0%B8-%D0%BD%D0%B0%D0%B3%D1%80%D0%B0%D0%B4%D1%8B-%D0%BD%D0%B0-ispo/" role="button">Читать »</a></p>
          <hr>

          <!-- REVIEW -->
          <a href="http://to4ek.net/category/%D0%BE%D0%B1%D0%B7%D0%BE%D1%80%D1%8B/" class="btn btn-lg btn-warning index-title">ОБЗОРЫ</a>
          <div class="clearfix"></div>
          <a href="http://to4ek.net/%D1%82%D0%B5%D1%81%D1%82-inov-8-f-lite-192/">
            <img src="http://to4ek.net/wp-content/uploads/2014/01/p7040662.jpg" alt="">
          </a>
          <div class="clearfix"></div>
          <a class="title3" href="http://to4ek.net/%D1%82%D0%B5%D1%81%D1%82-inov-8-f-lite-192/">Тест Inov-8 f-lite 192</a>
          <div class="clearfix"></div>
          <time class="text-muted" datetime="2014-01-20">23 Янв, 2014</time>
          <p>Первая проблема, которая встает перед покупкой кроссовок Inov-8, это выбор модели. Часто модели одной линейки имеет братьев сестер минимально отличающихся между собой. Кроссовки выбирались для бега по асфальту и хотелось попробовать проникнуться идеей естественного бега, на базе которой и конструируются все продукты Inov-8. </p>
          <p><a class="btn btn-default" href="http://to4ek.net/%D1%82%D0%B5%D1%81%D1%82-inov-8-f-lite-192/" role="button">Читать »</a></p>
          <hr>

        </div>
        <div class="col-md-4">

          <!-- STORY -->
          <a href="http://to4ek.net/category/%D0%BE%D1%82%D1%87%D0%B5%D1%82%D1%8B/" class="btn btn-lg btn-info index-title">ОТЧЕТЫ</a>
          <div class="clearfix"></div>
          <a href="http://to4ek.net/wilcze-gronie/">
            <img src="http://to4ek.net/wp-content/uploads/2014/02/1604746_527883723993041_926930671_n1.jpg" alt="">
          </a>
          <div class="clearfix"></div>
          <a class="title3" href="http://to4ek.net/wilcze-gronie/">Wilcze Gronie</a>
          <div class="clearfix"></div>
          <time class="text-muted" datetime="2014-01-20">12 Фев, 2014</time>
          <p>На этих выходных мы пробовали свои силы на Wilcze Gronie, это горная быстрая дистанция в польском Бескиде, забег на 15км с 700 метрами набора. Обычно такой набор встречается на более длинных дистанциях, где бежать быстро нет смысла, силы нужно экономить на всю гонку, тут же, нужно было от самого старта до финиша постоянно жать в газ.</p>
          <p><a class="btn btn-default" href="http://to4ek.net/wilcze-gronie/" role="button">Читать »</a></p>
          <hr>
          

          <!-- ARTICLES -->
          <a href="http://to4ek.net/category/%D1%81%D1%82%D0%B0%D1%82%D1%8C%D0%B8/" class="btn btn-lg btn-article index-title">СТАТЬИ</a>
          <div class="clearfix"></div>
          <a href="http://to4ek.net/%D0%B1%D0%B5%D0%B3-%D0%B1%D0%BE%D1%81%D0%B8%D0%BA%D0%BE%D0%BC/">
            <img src="http://to4ek.net/wp-content/uploads/2014/02/671efe2c61a29af91546456c7b8c20e8_watermarked.jpg" alt="">
          </a>
          <div class="clearfix"></div>
          <a class="title3" href="http://to4ek.net/%D0%B1%D0%B5%D0%B3-%D0%B1%D0%BE%D1%81%D0%B8%D0%BA%D0%BE%D0%BC/">О беге босиком</a>
          <div class="clearfix"></div>
          <time class="text-muted" datetime="2014-01-20">12 Фев, 2014</time>
          <p>Со второй половины 20-го века, было много научных и медицинских споров о пользе и вреде бега босиком. 1970-е годы, в частности, в западных странах увидели возрождающийся интерес к бегу трусцой. Тогда были разработаны и появились в продаже современные беговые кроссовки.</p>
          <p><a class="btn btn-default" href="http://to4ek.net/%D0%B1%D0%B5%D0%B3-%D0%B1%D0%BE%D1%81%D0%B8%D0%BA%D0%BE%D0%BC/" role="button">Читать »</a></p>
          <hr>
        </div>
        <div class="col-md-4">
          <!-- Поиск пока не работает
            <div class="input-group search-line">
              <input type="text" class="form-control" placeholder="Поиск" />
              <span class="input-group-btn">
                <button class="btn btn-default" type="button">Go!</button>
              </span>
            </div>
          -->
          <a href="/shop" class="btn btn-lg btn-danger index-title">МАГАЗИН</a>
          <div class="clearfix"></div>
          <ul class="media-list">
            <li class="media">
              <a class="pull-left" href="">
                <img class="media-object img-responsive" width="100" src="http://to4ek.net/wp-content/uploads/2014/01/pol_pl_Buty-inov-8-f-lite-192-zolto-czerwone-1059_4-300x168.jpg" alt="Inov-8 f-lite 192">
              </a>
              <div class="media-body">
                <a href="http://to4ek.net/shop/inov-8-f-lite-192/" class="title4 media-heading">Inov-8 f-lite 192</a>
                <p>Очень легкие кроссовки, в которых вы можете пойти дальше и гонки на дороге и выйти из города.</p>
              </div>
              <hr>
            </li>
            <li class="media">
              <a class="pull-left" href="http://to4ek.net/shop/inov-8-oroc-280-2013/">
                <img class="media-object img-responsive" width="100" src="http://to4ek.net/wp-content/uploads/2013/12/oroc280-300x170.jpg" alt="Inov-8 Oroc 280">
              </a>
              <div class="media-body">
                <a href="http://to4ek.net/shop/inov-8-oroc-280-2013/" class="title4 media-heading">Inov-8 Oroc 280</a>
                <p>Обновленная версия Inov-8 Oroc 280, крепких кроссовок для ориентирования с металическим шипом</p>
              </div>
              <hr>
            </li>
            <li class="media">
              <a class="pull-left" href="http://to4ek.net/shop/inov-8-oroc-340-2013/">
                <img class="media-object img-responsive" width="100" src="http://to4ek.net/wp-content/uploads/2014/01/1158_1-300x180.jpg" alt="Inov-8 Oroc 340">
              </a>
              <div class="media-body">
                <a href="http://to4ek.net/shop/inov-8-oroc-340-2013/" class="title4 media-heading">Inov-8 Oroc 340</a>
                <p>Последняя версия крепких кроссовок Oroc 340, созданных специально для ориентирования.</p>
              </div>
              <hr>
            </li>
            <li class="media">
              <a class="pull-left" href="http://to4ek.net/shop/inov-8-trailroc-255/">
                <img class="media-object img-responsive" width="100" src="http://to4ek.net/wp-content/uploads/2014/01/pol_pl_Buty-inov-8-trailroc-255-czarno-czerwone-841_1-300x138.jpg" alt="Inov-8 trailroc 255">
              </a>
              <div class="media-body">
                <a href="http://to4ek.net/shop/inov-8-trailroc-255/" class="title4 media-heading">Inov-8 trailroc 255</a>
                <p>Модель 2013 года, создана для тренировок и соревнований на длительныx дистанциях.</p>
              </div>
              <hr>
            </li>
            <li class="media">
              <a class="pull-left" href="http://to4ek.net/shop/inov-8-raceshell-220/">
                <img class="media-object img-responsive" width="100" src="http://to4ek.net/wp-content/uploads/2014/01/826_2-291x300.jpg" alt="Inov-8 Raceshell 220">
              </a>
              <div class="media-body">
                <a href="http://to4ek.net/shop/inov-8-raceshell-220/" class="title4 media-heading">Inov-8 raceshell 220</a>
                <p>Непромокаемая куртка на молнии, внешний слой из Ripstop и мембраны из PTFE.</p>
              </div>
              <hr>
            </li>
            <li class="media">
              <a class="pull-left" href="http://to4ek.net/shop/inov-8-race-elite-150-stormshell/">
                <img class="media-object img-responsive" width="100" src="http://to4ek.net/wp-content/uploads/2014/02/pol_pl_Kurtka-przeciwdeszczowa-inov-8-race-elite-150-stormshell-niebiesko-limonkowa-1075_1-246x300.jpg">
              </a>
              <div class="media-body">
                <a href="http://to4ek.net/shop/inov-8-race-elite-150-stormshell/" class="title4 media-heading">Inov-8 race elite 150 stormshell</a>
                <p>Суперлегкая, дышащая, и полностью водонепроницаемая ветровка. Оставайся всегда сухим, быстрым и легким. </p>
              </div>
            </li>
          </ul>
<!-- 
          <p>Это вопрос личных предпочтений при выборе обуви, но я попробую сравнить две популярные модели X-Talon 212 и Mudclaw 265. В X-Talon вы чувствуете шиповки крепко и уютно на ноге. Если вы не любите узкие кроссовки или у вас широкая нога то Mudclaw для вас.</p>
          <div class="row social-icos">
            <div class="col-md-12">
              <a href="" class="social_ico vk"></a>
              <a href="" class="social_ico google"></a>
              <a href="" class="social_ico inst"></a>
              <a href="" class="social_ico fb"></a>
            </div>
          </div>
           -->
        </div>
      </div>

    </div> <!-- /container -->
    
    <div id="footer">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <p class="text-muted">&copy; TO4EK.NET 2014, Все права защищены.</p>
          </div>
          <div class="col-md-6 ">
            <p class="social-icos smico text-muted pull-right">
              <a href="mailto:info@to4ek.net" class="address">info@to4ek.net</a>
              <a href="http://vk.com/to4ek_net" target="_blank" class="social_ico vk"></a>
              <a href="" class="social_ico google"></a>
              <a href="" class="social_ico inst"></a>
              <a href="" class="social_ico fb"></a>
            </p>
          </div>
        </div>
      </div>
    </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
  </body>
</html>
